/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.midterm_stationery;

/**
 *
 * @author f
 */
public class Eraser {

    private String eraserColor;
    private String erase;
    private double eraserSize;

    public Eraser(String eraserColor, String erase, double eraserSize) {
        this.eraserColor = eraserColor;
        this.erase = erase;
        this.eraserSize = eraserSize;
    }

    public void showEraser() {
        System.out.println("Eraser color: " + eraserColor);
        System.out.println("Erase: " + erase);
        System.out.println("Eraser size: " + eraserSize);
        System.out.println("__________________");
    }

    public String getEraserColor() {
        return eraserColor;
    }

    public String getEraser() {
        return erase;
    }

    public double getEraserSize() {
        return eraserSize;
    }
}
