/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.midterm_stationery;

/**
 *
 * @author f
 */
public class MainProgram {
    public static void main(String[] args) {
        Pen red = new Pen("Red", "Red", 0.5);
        red.showPen();
        Eraser blue = new Eraser("Blue", "Pencil", 5);
        blue.showEraser();
        Pencil green = new Pencil("Green", "HB");
        green.showPencil();
    }
    
}
