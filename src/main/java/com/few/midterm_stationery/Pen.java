/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.midterm_stationery;

/**
 *
 * @author f
 */
public class Pen {

    private String inkColor;
    private String penColor;
    private double nibSize; //nib = ปลายปากกา

    public Pen(String inkColor, String penColor, double nibSize) {
        this.inkColor = inkColor;
        this.penColor = penColor;
        this.nibSize = nibSize;
    }

    public void showPen() {
        System.out.println("Pen color: " + penColor);
        System.out.println("Ink color: " + inkColor);
        System.out.println("Nib size: " + nibSize);
        System.out.println("__________________");
    }

    public String getInkColor() {
        return inkColor;
    }

    public String getPenColor() {
        return penColor;
    }

    public double getNibSize() {
        return nibSize;
    }
}
