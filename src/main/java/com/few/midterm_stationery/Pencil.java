/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.midterm_stationery;

/**
 *
 * @author f
 */
public class Pencil {

    private String pencilColor;
    private String intensity; // intensity = ความเข้มของไส้ดินสอ

    public Pencil(String pencilColor, String intensity) {
        this.pencilColor = pencilColor;
        this.intensity = intensity;
    }
    public void showPencil(){
        System.out.println("Pencil color: " + pencilColor);
        System.out.println("Intensity: " + intensity);
        System.out.println("__________________");
    }
}
